#ifndef USERAGENTPROVIDER_H
#define USERAGENTPROVIDER_H

#include <QWebPage>
#include <QString>
#include <QDesktopServices>
#include <QUrl>
#include <QNetworkConfigurationManager>
//#include <QNetworkSession>

class UserAgentProvider : public QWebPage
{
    Q_OBJECT
public:
    explicit UserAgentProvider(QWidget *parent = 0);
    QString getUserAgent();

signals:

public slots:

};

class Helper : public QObject
{
    Q_OBJECT
public:
    explicit Helper(QObject *parent = 0);

//    Q_INVOKABLE void openNetConnection();
//    Q_INVOKABLE void closeNetConnection();
    Q_INVOKABLE void openURLDefault(const QString &url);
    Q_INVOKABLE void logToFile(const QString &messageToLog);
#ifdef Q_OS_SYMBIAN
    void LaunchBrowserL(const TDesC& aUrl, TUid& aUid);
#endif
private:
    QNetworkConfigurationManager *configManager;
//    QNetworkSession *netSession;
//signals:
//    void nwStateChanged ( bool isOnline );

//public slots:
//    void updateNwState (bool isOnline);
};
#endif // USERAGENTPROVIDER_H
