<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <source>About Application</source>
        <translation>Über gNewsReader</translation>
    </message>
    <message>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <source>Version 1.50</source>
        <translation type="obsolete">Version 1.50</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client für Symbian^3.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation type="obsolete">Open Source Google Reader Client für Symbian^3.&lt;br&gt;&lt;br&gt;Für Fehlermeldungen, Feedback und Anfragen &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Gehe zur Porjekt-Website&lt;/a&gt; oder schreibe dem Autor eine E-Mail &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <source>Project Team</source>
        <translation>Projekt Team</translation>
    </message>
    <message>
        <source>Author (twitter: @yogeshwarp)</source>
        <translation type="obsolete">Autor (twitter: @yogeshwarp)</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;(twitter: @gx_saurav)</source>
        <translation>UI Feedback &amp; Optimierungen / Artwork&lt;br&gt;(twitter: @gx_saurav)</translation>
    </message>
    <message>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <source>Polish Language (twitter: @pagaw102)</source>
        <translation>Polnisch (twitter: @pagaw102)</translation>
    </message>
    <message>
        <source>German Language</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <source>Bulgarian Language (twitter: @acnapyx)</source>
        <translation>Bulgarisch (twitter: @acnapyx)</translation>
    </message>
    <message>
        <source>Finnish Language</source>
        <translation>Finnisch</translation>
    </message>
    <message>
        <source>Version 1.60 Beta</source>
        <translation type="obsolete">Version 1.60 Beta</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Open Source Google Reader Client für Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;Für Fehlermeldungen, Feedback und Featureanfragen &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;gehe auf die Projektseite&lt;/a&gt; oder schreibe dem Autor eine E-Mail an &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <source>Privacy Policy</source>
        <translation>Datenschutzerklärung</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation>UI Feedback &amp; Optimierungen / Artwork&lt;br&gt;</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation>Vereinfachtes chinesisch</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation>Traditionelles chinesisch</translation>
    </message>
    <message>
        <source>Bulgarian Language</source>
        <translation>Bulgarisch</translation>
    </message>
    <message>
        <source>Croatian Language</source>
        <translation>Kroatisch</translation>
    </message>
    <message>
        <source>Polish Language</source>
        <translation>Polnisch</translation>
    </message>
    <message>
        <source>Italian Language</source>
        <translation>Italienisch</translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <source>Add New Feed to Google Reader</source>
        <translation>Füge dem Google Reader einen neuen Feed hinzu</translation>
    </message>
    <message>
        <source>Feed URL/Search Term</source>
        <translation>Feed URL/Suchbegriff</translation>
    </message>
    <message>
        <source>Title (Optional)</source>
        <translation>Titel (optional)</translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>About gNewsReader</source>
        <translation>Über gNewsReader</translation>
    </message>
    <message>
        <source>Use Light Theme</source>
        <translation>Helleres &quot;Theme&quot; nutzen</translation>
    </message>
    <message>
        <source>Unread Filter Global</source>
        <translation>Globaler Filter für ungelesene Artikel</translation>
    </message>
    <message>
        <source>Auto Image Resize</source>
        <translation>Bilder automatisch anpassen</translation>
    </message>
    <message>
        <source>Full HTML Content</source>
        <translation>Kompletten HTML-Inhalt</translation>
    </message>
    <message>
        <source>Use Bigger Fonts</source>
        <translation>Grössere Schrift verwenden</translation>
    </message>
    <message>
        <source>Auto Load Images</source>
        <translation>Bilder automatisch runterladen</translation>
    </message>
    <message>
        <source>Enable Swipe Gesture</source>
        <translation>Swipe-Gesten aktivieren</translation>
    </message>
    <message>
        <source>Export OPML file (Requires Google Login)</source>
        <translation>OPML-Datei exportieren (Google Login erforderlich)</translation>
    </message>
    <message>
        <source>Clear Authorization Data</source>
        <translation>Autorisierungs-Daten löschen</translation>
    </message>
    <message>
        <source>Dark Article Theme</source>
        <translation>Dunkle Artikelansicht</translation>
    </message>
</context>
<context>
    <name>CommonQueryDialog</name>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <source>Add or Edit Tags</source>
        <translation>Tag hinzufügen oder bearbeiten</translation>
    </message>
    <message>
        <source>Separate Tags by commas</source>
        <translation>Tags durch Kommas trennen</translation>
    </message>
    <message>
        <source>Enter tags here</source>
        <translation>Hier die Tags eingeben</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <source> of </source>
        <translation> von </translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <source>Mark as Read</source>
        <translation>Als gelesen markieren</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>Artikel als ungelesen markieren</translation>
    </message>
    <message>
        <source>Remove Star</source>
        <translation>Markierung entfernen</translation>
    </message>
    <message>
        <source>Add Star</source>
        <translation>Markierung hinzufügen</translation>
    </message>
    <message>
        <source>Send To</source>
        <translation>Senden an</translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <source>Search Results</source>
        <translation>Suchergebnisse</translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation>Abonnieren</translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation>Links, die ein neues Fenster erfordern, werden zur Zeit nicht unterstützt. Wenn du versuchst einen Google-Account zu erstellen, dann nutze bitte den Webbrowser auf deinem Gerät</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <source>Add Feed to Folder</source>
        <translation>Füge dem Ordner einen Feed hinzu</translation>
    </message>
    <message>
        <source>Enter New Folder Name</source>
        <translation>Neuen Ordnernamen eingeben</translation>
    </message>
    <message>
        <source>Go</source>
        <translation>Los</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
</context>
<context>
    <name>OAuthConstants</name>
    <message>
        <source>ago</source>
        <translation>vor</translation>
    </message>
    <message>
        <source>From Now</source>
        <translation>Seit jetzt</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>Jetzt gerade</translation>
    </message>
    <message>
        <source>min</source>
        <translation>Minute</translation>
    </message>
    <message>
        <source>mins</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <source>hr</source>
        <translation>Stunde</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <source>day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <source>days</source>
        <translation>Tagen</translation>
    </message>
    <message>
        <source>wk</source>
        <translation>Woche</translation>
    </message>
    <message>
        <source>wks</source>
        <translation>Wochen</translation>
    </message>
    <message>
        <source>mth</source>
        <translation>Monat</translation>
    </message>
    <message>
        <source>mths</source>
        <translation>Monaten</translation>
    </message>
    <message>
        <source>yr</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <source>yrs</source>
        <translation>Jahre</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <comment>e.g. %1 is number value such as 2, %2 is mins, %3 is ago</comment>
        <translation>%3 %1 %2</translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <source>Pull down to Refresh</source>
        <translation>Zum Aktualisieren runterziehen</translation>
    </message>
    <message>
        <source>Click to Refresh</source>
        <translation>Zum Aktualisieren anklicken</translation>
    </message>
    <message>
        <source>Release to Refresh</source>
        <translation>Zum Aktualisieren loslassen</translation>
    </message>
    <message>
        <source>Pull up to Load More</source>
        <translation>Hochziehen um weitere Artikel zu laden</translation>
    </message>
    <message>
        <source>Release to Load More</source>
        <translation>Loslassen um weitere Artikel zu laden</translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <source>Login to </source>
        <translation>Anmelden an </translation>
    </message>
    <message>
        <source>Read It Later</source>
        <translation>Read It Later</translation>
    </message>
    <message>
        <source>Instapaper</source>
        <translation>Instapaper</translation>
    </message>
    <message>
        <source>Login Id</source>
        <translation>Login-ID</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Save Login Information</source>
        <translation>Login-Informationen speichern</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Senden</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <source>Pocket</source>
        <translation>Pocket</translation>
    </message>
    <message>
        <source>Share Article with URL</source>
        <translation>Artikel mit URL teilen</translation>
    </message>
    <message>
        <source>Privacy Policy</source>
        <translation>Datenschutzerklärung</translation>
    </message>
    <message>
        <source>Enter Text Here</source>
        <translation>Text hier eingeben</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <source>Rename Feed</source>
        <translation>Feed umbenennen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <source>Unsubscribe</source>
        <translation>Feed abbestellen</translation>
    </message>
    <message>
        <source>Mark All as Read</source>
        <translation>Alle Artikel als gelesen markieren</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Move to Folder</source>
        <translation>In einen Ordner verschieben</translation>
    </message>
    <message>
        <source>Confirm Action</source>
        <translation>Vorgang bestätigen</translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <source>Subscriptions</source>
        <translation>Abonnements</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation>Verbindungsfehler. Fehler-Code:</translation>
    </message>
    <message>
        <source>Error in connection to Google</source>
        <translation>Fehler in der Verbindung zu Google</translation>
    </message>
    <message>
        <source>Google Sign-In</source>
        <translation>Google Anmeldung</translation>
    </message>
    <message>
        <source>Updating Subscriptions..</source>
        <translation>Abonnements aktualisieren..</translation>
    </message>
    <message>
        <source>All News</source>
        <translation>Alle News</translation>
    </message>
    <message>
        <source>All Unread</source>
        <translation>Alle ungelesenen</translation>
    </message>
    <message>
        <source>All Starred</source>
        <translation>Alle markierten</translation>
    </message>
    <message>
        <source>Loading Feed..</source>
        <translation>Feed wird geladen..</translation>
    </message>
    <message>
        <source> of </source>
        <translation> von </translation>
    </message>
    <message>
        <source>You have no matching Feed Items</source>
        <translation>Es gibt keine passenden Artikel</translation>
    </message>
    <message>
        <source>You have no subscribed feeds</source>
        <translation>Es gibt keine abonnierten Feeds</translation>
    </message>
    <message>
        <source>No Feeds with Unread Items</source>
        <translation>Keine Feed mit ungelesenen Artikeln</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service</source>
        <translation>Link erfolgreich an den Service gesendet</translation>
    </message>
    <message>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation>Anmeldung am Service nicht erfolgreich. Der Benutzername oder das Passwort sind falsch</translation>
    </message>
    <message>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation>Unbekannter Fehler bei der Übertragung. Bitte in ein paar Minuten erneut probieren</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (params)</source>
        <translation>Link erfolgreich an den Service (params) gesendet.</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation>Link erfolgreich an (%1) gesendet</translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <source>Subscribe to New Feed</source>
        <translation>Neuen Feed abonnieren</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation>Alle Feeds anzeigen</translation>
    </message>
    <message>
        <source>Show Unread</source>
        <translation>Nur Feeds mit ungelesenen A. anzeigen</translation>
    </message>
    <message>
        <source>Load Starred Feeds</source>
        <translation>Alle markierten Artikel anzeigen</translation>
    </message>
    <message>
        <source>Load New Feeds</source>
        <translation>Alle ungelesenen Artikel anzeigen</translation>
    </message>
    <message>
        <source>Load All Feeds</source>
        <translation>Alle Artikel (auch gelesene) anzeigen</translation>
    </message>
    <message>
        <source>Clear Authorization Data</source>
        <translation>Autorisierungs-Daten löschen</translation>
    </message>
    <message>
        <source>Mark All as Read</source>
        <translation>Alle Artikel als gelesen markieren</translation>
    </message>
    <message>
        <source>Load More Items..</source>
        <translation>Weitere Artikel laden..</translation>
    </message>
    <message>
        <source>Show All Items</source>
        <translation>Alle Artikel anzeigen</translation>
    </message>
    <message>
        <source>Show Unread Items</source>
        <translation>Nur ungelesene Artikel anzeigen</translation>
    </message>
    <message>
        <source>Open in Browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
    <message>
        <source>Copy URL</source>
        <translation>URL kopieren</translation>
    </message>
    <message>
        <source>Undo Keep Unread</source>
        <translation>Als ungelesen behalten rückgängig machen</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>Als ungelesen behalten</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Teilen</translation>
    </message>
    <message>
        <source>Google</source>
        <translation>Google</translation>
    </message>
    <message>
        <source>Confirm Action</source>
        <translation>Vorgang bestätigen</translation>
    </message>
    <message>
        <source>Mark all as Read</source>
        <translation>Alle Artikel als gelesen markieren</translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <source>Your tweet was posted successfully</source>
        <translation>Dein Tweet war erfolgreich</translation>
    </message>
    <message>
        <source>Your article was posted successfully to Instapaper</source>
        <translation>Der Artikel wurde erfolgreich an Instapaper übermittelt</translation>
    </message>
    <message>
        <source>Your post was shared on Facebook successfully</source>
        <translation>Dein Post wurde erfolgreich an Facebook übermittelt</translation>
    </message>
    <message>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation>Fehler bei der Authentifizierung: bitte erneut einloggen</translation>
    </message>
    <message>
        <source>Unknows Error: Please retry after some time</source>
        <translation>Unbekannter Fehler: bitte in ein paar Minuten erneut probieren</translation>
    </message>
    <message>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation>Twitter-Autorisierung erfolgreich gelöscht</translation>
    </message>
    <message>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation>Instapaper-Autorisierung erfolgreich gelöscht</translation>
    </message>
    <message>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation>Facebook-Autorisierung erfolgreich gelöscht</translation>
    </message>
    <message>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation>Pocket-Autorisierung erfolgreich gelöscht</translation>
    </message>
</context>
</TS>
