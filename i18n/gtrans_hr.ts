<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="hr_HR">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="13"/>
        <source>About Application</source>
        <translation>O aplikaciji</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="51"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="52"/>
        <source>Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="56"/>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="63"/>
        <source>Privacy Policy</source>
        <translatorcomment>via Google Translate</translatorcomment>
        <translation type="unfinished">Izjava o privatnosti</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="97"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="109"/>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="198"/>
        <source>Polish Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="126"/>
        <source>Simplified Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="138"/>
        <source>Traditional Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="162"/>
        <source>Bulgarian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="186"/>
        <source>Croatian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="210"/>
        <source>Italian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version 1.60 Beta</source>
        <translation>Verzija 1.60 Beta</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client for Symbian^3.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Open source Google Reader klijent za Symbian^3&lt;br&gt;&lt;br&gt;Za prijavu bugova, dojmove i zahtjeve za novim funkcionalnostima &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;otvorite web site projekta&lt;/a&gt; ili pošaljite e-mail autoru na adresu &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="76"/>
        <source>Project Team</source>
        <translation>Ekipa iza projekta</translation>
    </message>
    <message>
        <source>Author (twitter: @yogeshwarp)</source>
        <translation>Autor (twitter: @yogeshwarp)</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;(twitter: @gx_saurav)</source>
        <translation>Povratne informacije vezane uz sučelje / ilustracije&lt;br&gt;(twitter: @gx_saurav)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="116"/>
        <source>Translators</source>
        <translation>Prevoditelji</translation>
    </message>
    <message>
        <source>Polish Language (twitter: @pagaw102)</source>
        <translation>Poljski jezik (twitter: @pagaw102)</translation>
    </message>
    <message>
        <source>Simplified Chinese (twitter: @yeatse)</source>
        <translation>Pojednostavljeni kineski (twitter: @yeatse)</translation>
    </message>
    <message>
        <source>Traditional Chinese (twitter: @garykb8)</source>
        <translation>Tradicionalni kineski (twitter: @garykb8)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="150"/>
        <source>German Language</source>
        <translation>Nemački jezik</translation>
    </message>
    <message>
        <source>Bulgarian Language (twitter: @acnapyx)</source>
        <translation>Bugarski jezik (twitter: @acnapyx)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="174"/>
        <source>Finnish Language</source>
        <translation>Finski jezik</translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="6"/>
        <source>Add New Feed to Google Reader</source>
        <translation>Dodaj novi feed u Google reader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="21"/>
        <source>Feed URL/Search Term</source>
        <translation>Adresa feed-a / Pojam pretrage</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="27"/>
        <source>Title (Optional)</source>
        <translation>Naslov (opcionalno)</translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="12"/>
        <source>Settings</source>
        <translation>Postavke</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="36"/>
        <source>About gNewsReader</source>
        <translation>O gNewsReader-u</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="41"/>
        <source>Use Light Theme</source>
        <translation>Koristi svijetlu temu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="57"/>
        <source>Unread Filter Global</source>
        <translation>Globalni filter na nepročitano</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="73"/>
        <source>Auto Image Resize</source>
        <translation>Automatsko promjena veličine slika</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="89"/>
        <source>Full HTML Content</source>
        <translation>Puni HTML sadržaj</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="105"/>
        <source>Use Bigger Fonts</source>
        <translation>Koristi veći font</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="121"/>
        <source>Auto Load Images</source>
        <translation>Automatsko učitavanje slika</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="137"/>
        <source>Enable Swipe Gesture</source>
        <translation>Omogući swipe geste</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="168"/>
        <source>Clear Authorization Data</source>
        <translation>Poništi podatke o autorizaciji</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="210"/>
        <source>Dark Article Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="225"/>
        <source>Export OPML file (Requires Google Login)</source>
        <translation>Izvoz OPML datoteke (zahtjeva Google login)</translation>
    </message>
</context>
<context>
    <name>CommonQueryDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="9"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="10"/>
        <source>Cancel</source>
        <translation>Odustani</translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="6"/>
        <source>Add or Edit Tags</source>
        <translation>Dodaj ili izmijeni oznake</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="22"/>
        <source>Separate Tags by commas</source>
        <translation>Razdvoji oznake zarezima</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="27"/>
        <source>Enter tags here</source>
        <translation>Ovdje unesi oznake</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="46"/>
        <source>Save</source>
        <translation>Spremi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="58"/>
        <source>Cancel</source>
        <translation>Odustani</translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="78"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="155"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="172"/>
        <source> of </source>
        <translation>od</translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="21"/>
        <source>Mark as Read</source>
        <translation>Označi kao pročitano</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="32"/>
        <source>Keep Unread</source>
        <translation>Zadrži nepročitanim</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Remove Star</source>
        <translation>Ukloni zvjezdicu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Add Star</source>
        <translation>Dodaj zvjezdicu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="47"/>
        <source>Send To</source>
        <translation>Pošalji na</translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="18"/>
        <source>Search Results</source>
        <translation>Rezultati pretraživanja</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="98"/>
        <source>Subscribe</source>
        <translation>Pretplati se</translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="178"/>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation>Otvaranje linkova koji otvaraju nove prozore trenutačno nije podržano. Ako pokušavate obnoviti/izraditi novi Google račun molimo iskoristite web browser na vašem telefonu za to.</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="190"/>
        <source>Close</source>
        <translation>Zatvori</translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="6"/>
        <source>Add Feed to Folder</source>
        <translation>Dodaj novi feed u mapu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="36"/>
        <source>Enter New Folder Name</source>
        <translation>Unesi novo ime foldera</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="41"/>
        <source>Go</source>
        <translation>Idi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="108"/>
        <source>Close</source>
        <translation>Zatvori</translation>
    </message>
</context>
<context>
    <name>OAuthConstants</name>
    <message>
        <source>ago</source>
        <translation>star</translation>
    </message>
    <message>
        <source>From Now</source>
        <translation>Od sada</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>upravo</translation>
    </message>
    <message>
        <source>min</source>
        <translation>minuta</translation>
    </message>
    <message>
        <source>mins</source>
        <translation>minute</translation>
    </message>
    <message>
        <source>hr</source>
        <translation>sat</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>sati</translation>
    </message>
    <message>
        <source>day</source>
        <translation>dan</translation>
    </message>
    <message>
        <source>days</source>
        <translation>dani</translation>
    </message>
    <message>
        <source>wk</source>
        <translation>tjedan</translation>
    </message>
    <message>
        <source>wks</source>
        <translation>tjedana</translation>
    </message>
    <message>
        <source>mth</source>
        <translation>mjesec</translation>
    </message>
    <message>
        <source>mths</source>
        <translation>mjeseci</translation>
    </message>
    <message>
        <source>yr</source>
        <translation>godina</translation>
    </message>
    <message>
        <source>yrs</source>
        <translation>godina</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <comment>e.g. %1 is number value such as 2, %2 is mins, %3 is ago</comment>
        <translation>%1 %2 %3</translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull down to Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull up to Load More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Load More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Login to </source>
        <translation>Prijava na</translation>
    </message>
    <message>
        <source>Read It Later</source>
        <translation>Read It Later</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Instapaper</source>
        <translation>Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Pocket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="11"/>
        <source>Share Article with URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="35"/>
        <source>Login Id</source>
        <translation>Korisnička oznaka</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="42"/>
        <source>Password</source>
        <translation>lozinka</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="51"/>
        <source>Save Login Information</source>
        <translation>Spremi korisničke podatke</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="57"/>
        <source>Privacy Policy</source>
        <translation type="unfinished">Izjava o privatnosti</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="81"/>
        <source>Enter Text Here</source>
        <translation>Unesi tekst ovdje</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Send</source>
        <translation>Šalji</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="160"/>
        <source>Cancel</source>
        <translation>Odustani</translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="6"/>
        <source>Rename Feed</source>
        <translation>Preimenuj feed</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="40"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation>Odustani</translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="50"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Unsubscribe</source>
        <translation>Otkaži pretplatu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="32"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <source>Mark All as Read</source>
        <translation>Označi sve kao pročitano</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Confirm Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="36"/>
        <source>Rename</source>
        <translation>Preimenuj</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="45"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <source>Delete</source>
        <translation>Obriši</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="55"/>
        <source>Move to Folder</source>
        <translation>Premjesti u mapu</translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/SubscriptionsPage.qml" line="42"/>
        <source>Subscriptions</source>
        <translation>Pretplate</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="44"/>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation>Pogreška u vezi. Broj greške je:</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="116"/>
        <source>Error in connection to Google</source>
        <translation>Kreška prilikom povezivanja na Google</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="157"/>
        <source>Google Sign-In</source>
        <translation>Google prijava</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="176"/>
        <location filename="../qml/gNewsReader/js/main.js" line="186"/>
        <source>Updating Subscriptions..</source>
        <translation>Osvježavanje pretplata..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="333"/>
        <source>All News</source>
        <translation>Sve vijesti</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="337"/>
        <source>All Unread</source>
        <translation>Sve nepročitane</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="341"/>
        <source>All Starred</source>
        <translation>Sve sa zvjezdicom</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="350"/>
        <location filename="../qml/gNewsReader/js/main.js" line="358"/>
        <source>Loading Feed..</source>
        <translation>Učitavam feed..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="414"/>
        <source> of </source>
        <translation>od</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="454"/>
        <source>You have no matching Feed Items</source>
        <translation>Nema odgovarajućih stavki feed-a</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>You have no subscribed feeds</source>
        <translation>Nema pretplaćenih stavki feed-a</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>No Feeds with Unread Items</source>
        <translation>Nema feed-ova s nepročitanim stavkama</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="689"/>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (params)</source>
        <translation>Uspješno slanje linka servisu (parametri)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="692"/>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation>Neuspješna prijava na servis, korisničko ime ili lozinka možda nisu valjani</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="696"/>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation>Nepoznata pogreška prilikom slanja. Molimo pokušajte ponovo kasnije</translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="86"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="128"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <source>Go Online</source>
        <translation>Poveži se</translation>
    </message>
    <message>
        <source>Go Offline</source>
        <translation>Isključi se</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="216"/>
        <source>Subscribe to New Feed</source>
        <translation>Pretplati se na novi feed</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show All</source>
        <translation>Prikaži sve</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show Unread</source>
        <translation>Prikaži nepročitane</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="232"/>
        <source>Load Starred Feeds</source>
        <translation>Učitaj feedove s zvjezdicom</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="224"/>
        <source>Load New Feeds</source>
        <translation>Učitaj nove feedove</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="228"/>
        <source>Load All Feeds</source>
        <translation>Učitaj sve feedove</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="236"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <source>Clear Authorization Data</source>
        <translation>Poništi podatke o autorizaciji</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Confirm Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="290"/>
        <source>Mark All as Read</source>
        <translation>Označi sve kao pročitano</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Mark all as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="295"/>
        <source>Load More Items..</source>
        <translation>Učitaj još stavki</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show All Items</source>
        <translation>Prikaži sve stavke</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show Unread Items</source>
        <translation>Prikaži nepročitane stavke</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="371"/>
        <source>Open in Browser</source>
        <translation>Otvori u web pregledniku</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="375"/>
        <source>Copy URL</source>
        <translation>Kopiraj adresu</translation>
    </message>
    <message>
        <source>Undo Keep Unread</source>
        <translation>Poništi zadržavanje nepročitanog</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>Zadrži nepročitano</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Podijeli</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="443"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="38"/>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="44"/>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="49"/>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="55"/>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="75"/>
        <source>Your tweet was posted successfully</source>
        <translation>Tvoj tweet je uspješno poslan</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="95"/>
        <source>Your article was posted successfully to Instapaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="141"/>
        <source>Your post was shared on Facebook successfully</source>
        <translation>Tvoj unos je uspješno podijeljen na Facebooku</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="153"/>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="156"/>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation>Pogreška provjere identiteta: Molimo ponovo se autorizirajte s korisničkim imenom</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="157"/>
        <source>Unknows Error: Please retry after some time</source>
        <translation>Nepoznata greška: Molimo pokušajte ponovo kasnije</translation>
    </message>
</context>
</TS>
