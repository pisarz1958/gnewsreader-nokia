#ifndef NETWORKACCESSMANAGERFACTORY_H
#define NETWORKACCESSMANAGERFACTORY_H

#include <QDeclarativeNetworkAccessManagerFactory>
#include <QNetworkAccessManager>

class NetworkAccessManagerFactory : public QDeclarativeNetworkAccessManagerFactory
{
public:
    explicit NetworkAccessManagerFactory(QString p_userAgent = "");

    virtual QNetworkAccessManager* create(QObject* parent);

private:
    QString __userAgent;
};
#endif // NETWORKACCESSMANAGERFACTORY_H
