#include "useragentprovider.h"
#include <QDebug>
#include <QNetworkConfiguration>

#ifdef Q_OS_SYMBIAN
#include <apgtask.h>
#include <apgcli.h>
#include <EIKENV.H>
#endif

UserAgentProvider::UserAgentProvider(QWidget *parent) :
    QWebPage(parent)
{
}

QString UserAgentProvider::getUserAgent()
{
    return userAgentForUrl(QUrl(""));
}

Helper::Helper(QObject *parent) :
    QObject(parent)
{
    //qDebug() << "Helper is getting new lease of life";
    configManager = new QNetworkConfigurationManager(this);
    //QObject::connect(configManager, SIGNAL(onlineStateChanged(bool)), this, SLOT(updateNwState(bool)));
}

//void Helper::updateNwState(bool isOnline) {
//    //qDebug() << "Network state changed to:" << isOnline;
//    this->nwStateChanged(isOnline);
//}

//void Helper::openNetConnection() {
//    const bool canStartIAP = (configManager->capabilities()
//                              & QNetworkConfigurationManager::CanStartAndStopInterfaces);
//    // Is there default access point, use it
//    QNetworkConfiguration cfg = configManager->defaultConfiguration();
//    if (!cfg.isValid() || (!canStartIAP && cfg.state() != QNetworkConfiguration::Active)) {
//        qDebug() << "No Access Point found.";
//        return;
//    }

//    netSession = new QNetworkSession(cfg, this);
//    netSession->open();
//    netSession->waitForOpened(-1);
//}

//void Helper::closeNetConnection() {
//    if(netSession->isOpen()) netSession->stop();
//}

void Helper::openURLDefault(const QString &url){
#ifdef Q_OS_SYMBIAN
    const TInt KWmlBrowserUid = 0x10008D39; //10008d39
    TPtrC myUrl (reinterpret_cast<const TText*>(url.constData()),url.length());
    //QDesktopServices::openUrl(QUrl(url));
    RApaLsSession lsSession;
    // create a session with apparc server.
    User::LeaveIfError(lsSession.Connect());
    CleanupClosePushL(lsSession);
    TDataType mimeDatatype(_L8("application/x-web-browse"));
    TUid handlerUID;
    // get the default application uid for application/x-web-browse
    lsSession.AppForDataType(mimeDatatype,handlerUID);
    // there may not be a mime-type handler defined, especially on S60 3.x
    // in such case we default to the built-in browser
    if (handlerUID.iUid == 0 || handlerUID.iUid == -1)
            handlerUID.iUid = KWmlBrowserUid;
    // Finally launch default browser
    LaunchBrowserL(myUrl, handlerUID);
    qDebug() << "Launching with UID:" << handlerUID.iUid;
    // Cleanup
    CleanupStack::PopAndDestroy(&lsSession);
#else
    qDebug() << "Trying default Qt function : QDesktopServices::openUrl" << url;
    QDesktopServices::openUrl(url);
#endif
}

// ----------------------------------------------------
// CBrowserAppUi::LaunchBrowserL(const TDesC& aUrl)
// Used for launching the default browser with provided url.
// ----------------------------------------------------
//
#ifdef Q_OS_SYMBIAN
void Helper::LaunchBrowserL(const TDesC& aUrl, TUid& aUid)
{
        qDebug() << "Inside LaunchBrowserL";
        TApaTaskList taskList( CEikonEnv::Static()->WsSession() );
        TApaTask task = taskList.FindApp( aUid );
        if ( task.Exists() )
        {
                HBufC8* param = HBufC8::NewLC( aUrl.Length() + 2);
                //"4 " is to Start/Continue the browser specifying a URL
                param->Des().Append(_L("4 "));
                param->Des().Append(aUrl);
                task.SendMessage( TUid::Uid( 0 ), *param ); // Uid is not used
                CleanupStack::PopAndDestroy(param);
        }
        else
        {
                HBufC16* param = HBufC16::NewLC( aUrl.Length() + 2);
                //"4 " is to Start/Continue the browser specifying a URL
                param->Des().Append(_L("4 "));
                param->Des().Append(aUrl);
                RApaLsSession appArcSession;
                // connect to AppArc server
                User::LeaveIfError(appArcSession.Connect());
                TThreadId id;
                appArcSession.StartDocument( *param, aUid, id );
                appArcSession.Close();
                CleanupStack::PopAndDestroy(param);
        }
}
#endif

void Helper::logToFile(const QString &messageToLog){
    qDebug() << messageToLog;
}
