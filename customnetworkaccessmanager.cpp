#include "customnetworkaccessmanager.h"
#include "useragentprovider.h"
#include <QNetworkReply>
#include <QSslSocket>
#include <QSslConfiguration>

CustomNetworkAccessManager::CustomNetworkAccessManager(QString p_userAgent, QObject *parent) :
    QNetworkAccessManager(parent), __userAgent(p_userAgent)
{
}

QNetworkReply *CustomNetworkAccessManager::createRequest( Operation op,
                                                          const QNetworkRequest &req,
                                                          QIODevice * outgoingData )
{
    QSslConfiguration config = req.sslConfiguration();
    config.setPeerVerifyMode(QSslSocket::VerifyNone);
    config.setProtocol(QSsl::TlsV1);

    QNetworkRequest new_req(req);
    new_req.setSslConfiguration(config);
    new_req.setRawHeader("User-Agent", __userAgent.toAscii());

    //new_req.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);

    QNetworkReply *reply = QNetworkAccessManager::createRequest( op, new_req, outgoingData );
    return reply;
}

