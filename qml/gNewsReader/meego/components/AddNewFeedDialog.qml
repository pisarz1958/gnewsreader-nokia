import QtQuick 1.1
import com.nokia.meego 1.0
import ".." 1.1

CommonDialog {
    id: subFeedDialog
    titleText: qsTr("Add New Feed to Google Reader")

    property string feedUrl: ""
    property string feedFolder: ""

    content: Column {
        width: parent.width - 2*meegoStyle.paddingMedium
        height: feedUrlText.height + feedTitleText.height + 3*meegoStyle.paddingMedium
        spacing: meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        TextField {
            placeholderText: qsTr("Feed URL/Search Term")
            id: feedUrlText
            text: feedUrl
            width: parent.width
        }
        TextField {
            placeholderText: qsTr("Title (Optional)")
            id: feedTitleText
            text: feedFolder
            width: parent.width
            //platformStyle: TextFieldStyle { inverted: true }
        }
    }

    buttons: Row  {
        spacing: meegoStyle.paddingMedium
        width: parent.width - 2*meegoStyle.paddingMedium
        height: addNewFeedDialogButtonOk.height + meegoStyle.paddingMedium
        //exclusive: false
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        Button {
            width: (parent.width - 2*meegoStyle.paddingMedium)*0.2
            iconSource: "../../pics/tb_paste.svg"
            onClicked: feedUrlText.paste()
            platformStyle: ButtonStyle{ inverted: true }
        }
        Button {
            id: addNewFeedDialogButtonOk
            width: (parent.width - 2*meegoStyle.paddingMedium)*0.4
            iconSource: "../../pics/tb_ok.svg"
            platformStyle: ButtonStyle{ inverted: true }
            onClicked: {
                if(feedUrlText.text != undefined && feedUrlText.text != "") {
                    if(/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(feedUrlText.text)) {
                    subscrListPage.itemOptions("feed/"+feedUrlText.text, -1, "subscribe", feedTitleText.text)
                    subFeedDialog.close()
                    } else {
                        subscrListPage.searchForFeed(feedUrlText.text); subFeedDialog.close()
                    }
                }
            }
        }
//                Button {
//                    iconSource: "../pics/tb_search.svg"
//                    onClicked: { subscrListPage.searchForFeed(feedUrlText.text); subFeedDialog.close() }
//                }

        Button {
            width: (parent.width - 2*meegoStyle.paddingMedium)*0.4
            platformStyle: ButtonStyle{ inverted: true }
            iconSource: "../../pics/tb_close_stop.svg"
            onClicked: subFeedDialog.close()
        }
    }
    //onClickedOutside: { subFeedDialog.close() }
}
