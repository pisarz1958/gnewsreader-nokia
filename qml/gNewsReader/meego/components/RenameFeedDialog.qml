import QtQuick 1.1
import com.nokia.meego 1.0
import ".." 1.1

CommonDialog {
    id: editDialog
    titleText: qsTr("Rename Feed")

    property string editTitle: ""
    property string editFeedId: ""
    property string category: ""

    content: Column {
        width: parent.width - 2*meegoStyle.paddingMedium
        //height: editDialogText.height + 2*meegoStyle.paddingMedium
        spacing: meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        TextField {
            id: editDialogText
            text: editTitle
            width: parent.width
        }
    }

    buttons: Row  {
        width: parent.width - 2*meegoStyle.paddingMedium
        height: renameDialogButtonOk.height + meegoStyle.paddingMedium
        spacing: meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        Button {
            id: renameDialogButtonOk
            width: (parent.width - 2*meegoStyle.paddingMedium) * 0.5
            text: qsTr("Ok")
            platformStyle: ButtonStyle{ inverted: true }
            onClicked: {
                editDialog.close()
                if(editDialogText.text != undefined && editDialogText.text != "") {
                    category == "sub" ? subscrListPage.itemOptions(editFeedId, -1, "edit", editDialogText.text)
                                      : subscrListPage.itemOptions(editFeedId, -1, "rename", editDialogText.text)
                }
            }
        }
        Button {
            text: qsTr("Cancel")
            platformStyle: ButtonStyle{ inverted: true }
            width: (parent.width - 2*meegoStyle.paddingMedium) * 0.5
            onClicked: editDialog.close()
        }
    }
    //onClickedOutside: editDialog.close()
}
