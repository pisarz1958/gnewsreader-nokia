import QtQuick 1.1
import com.nokia.meego 1.0
import ".." 1.1

CommonDialog {
    id: edittagsDialog
    titleText: qsTr("Add or Edit Tags")
    height: editTagsCenterColumn.height + editTagsButtonRow.height + 3*meegoStyle.paddingMedium
    property string origTags: feedItemPage.getCurrentFeed() != null ? feedItemPage.getCurrentFeed().categoriesStr : ""

    content: Column {
        id: editTagsCenterColumn
        width: parent.width - 2*meegoStyle.paddingMedium
        height: editTagsDialogTextLabel.height + tagText.height + 3*meegoStyle.paddingMedium
        spacing: meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        ListItemText {
            id: editTagsDialogTextLabel
            text: qsTr("Separate Tags by commas")
            role: "Subtitle"
        }

        TextField {
            placeholderText: qsTr("Enter tags here")
            id: tagText
            text: currentTags
            width: parent.width
        }
    }

    buttons: Row  {
        id: editTagsButtonRow
        width: parent.width - 2*meegoStyle.paddingMedium
        height: btnEditTagsSave.height + meegoStyle.paddingMedium
        spacing: meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        Button {
            id: btnEditTagsSave
            text: qsTr("Save")
            width: (parent.width - 2*meegoStyle.paddingMedium)*0.5
            PlatFormStyle: ButtonStyle{ inverted: true }
            onClicked: {
                edittagsDialog.close()
                if((tagText.text != undefined && tagText.text != "") ||
                        (tagText.text =="" && edittagsDialog.origTags != undefined && edittagsDialog.origTags != "")) {
                    feedItemPage.saveTags(tagText.text, origTags) //Call edit tag action
                    //feedItemPage.categoriesStr = tagText.text
                }
            }
        }
        Button {
            text: qsTr("Cancel")
            width: (parent.width - 2*meegoStyle.paddingMedium)*0.5
            PlatFormStyle: ButtonStyle{ inverted: true }
            onClicked: edittagsDialog.close()
        }
    }
    //onClickedOutside: edittagsDialog.close()
    Component.onCompleted: { tagText.text = feedItemPage.getCurrentFeed().categoriesStr }
}
