import QtQuick 1.1
import com.nokia.meego 1.0
import "../" 1.0

Page {
    id: feedSearchResultPage
    property ListModel model
    property bool mainCompVisible : true

    signal subscribeToFeed(string feedid)
    signal addToList(string title, string snippet, string url)
    signal clearResults()

    anchors { fill: parent; topMargin: statusBar.height  + topMsgText.height; bottomMargin: toolBar.height }

    onAddToList: { searchResultListModel.append({"searchtitle": title, "searchsnippet":snippet, "searchurl": url}) }
    onClearResults: searchResultListModel.clear()
    onStatusChanged: if(status == PageStatus.Active) { topMsgText.text=qsTr("Search Results") }

    model: ListModel {
            id: searchResultListModel
        }

    Loader {
        id: feedSearchResultLoader
        sourceComponent: feedResultListComponent//undefined
        anchors { fill: parent; margins: 0 }
        focus: true
    }

    Component {
        id:feedResultListComponent
        Item {
            ListView {
                id: feedResultListView
                focus: true
                delegate: feedResultDelegate
                anchors { fill: parent; margins: 0 }
                model: feedSearchResultPage.model
            }
            ScrollDecorator {
                flickableItem: feedResultListView
            }
            Component.onCompleted: { feedResultListView.forceActiveFocus() }
        }
    }

    Component {
        id: feedResultDelegate

        ListItem {
            id: feedResultItem
            width: feedResultListView.width
            //platformInverted: window.useLightTheme
            implicitHeight: feedResultItemColumn.height + 2*meegoStyle.paddingMedium

            Column {
                id: feedResultItemColumn
                spacing: meegoStyle.paddingMedium
                anchors {
                    top: feedResultItem.top
                    left: feedResultItem.left
                    margins: meegoStyle.paddingMedium
                }
                width: feedResultItem.width - 2*meegoStyle.paddingMedium

                ListItemText {
                    id: searchResultTitle
                    text: searchtitle
                    width: parent.width
                    mode: feedResultItem.mode
                    role: "Title"
                    wrapMode: Text.Wrap
                    elide: Text.ElideNone
                }
                ListItemText {
                    id: searchResultSnippet
                    text: searchsnippet
                    mode: feedResultItem.mode
                    role: "SubTitle"
                    wrapMode: Text.Wrap
                    elide: Text.ElideNone
                    width: parent.width
                }
                ListItemText {
                    id: searchResultUrl
                    text: searchurl
                    mode: feedResultItem.mode
                    role: "SubTitle"
                    width: parent.width
                    wrapMode: Text.Wrap
                    elide: Text.ElideNone
                }
                Button {
                    text: qsTr("Subscribe")
                    onClicked: {
                        subscrListPage.itemOptions("feed/"+searchurl, -1, "subscribe", "")
                        subscrListPage.isCountDirty = true
                    }
                }
            }
        }
    }

    tools: ToolBarLayout {
        ToolIcon {
            iconId: "toolbar-back"
            onClicked: pageStack.depth == 1 ? Qt.quit() : pageStack.pop()
        }

        ToolIcon {
            iconSource: "../../pics/tb_about_app.svg"
            onClicked: {pageStack.push(Qt.resolvedUrl("AboutApplicationPage.qml"))}
        }
    }
}
