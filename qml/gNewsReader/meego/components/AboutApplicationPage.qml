import QtQuick 1.1
import com.nokia.meego 1.0
import ".." 1.1
import "../../js/OAuthConstants.js" as Const

Page {
    id: aboutAppPage
    property bool mainCompVisible : true
    property bool platformInverted : false

    anchors { fill: parent; topMargin: statusBar.height + topMsgText.height; bottomMargin: toolBar.height }
    onStatusChanged: {
        if(status == PageStatus.Active) { topMsgText.text=qsTr("About Application") }
    }

    Loader {
        id: aboutAppLoader
        sourceComponent: aboutAppComponent//undefined
        anchors { fill: parent; margins: meegoStyle.paddingLarge }
        focus: true
    }

    Component {
        id:aboutAppComponent
        Flickable {
            width: parent.width
            height: parent.height
            contentWidth: parent.width// - 2*meegoStyle.paddingLarge
            contentHeight: mainColumn.height
            boundsBehavior : Flickable.StopAtBounds

            Item {
                id: mainItem
                width: parent.width
                Column {
                    id: mainColumn
                    width: parent.width
                    spacing: meegoStyle.paddingMedium
                    Row {
                        spacing: meegoStyle.paddingMedium
                        Image {
                            id: gnewsReaderImg
                            source: "../../pics/gNewsReader.svg"
                            sourceSize.height: meegoStyle.graphicSizeLarge
                            sourceSize.width: meegoStyle.graphicSizeLarge
                        }

                        Column {
                            id: aboutAppHeaderColumn
                            anchors.verticalCenter: gnewsReaderImg.verticalCenter
                            Label {text: qsTr("gNewsReader"); anchors.horizontalCenter: aboutAppHeaderColumn.horizontalCenter }
                            ListItemText {text: qsTr("Version %1").arg(appVersion); anchors.horizontalCenter: aboutAppHeaderColumn.horizontalCenter; role: "SubTitle"}
                        }
                    }
                    Label {
                        text: qsTr("Open Source Google Reader Client for Symbian^3/MeeGo.<br><br>For Bug Reports, Feedback and Feature Requests <a href=\"https://projects.developer.nokia.com/gNewsReader\">Go to Project Website</a> or e-mail author <a href=\"mailto:yogeshwarp@gmail.com\">yogeshwarp@gmail.com</a><br><br>© 2011 Yogeshwar Padhyegurjar<br>")
                        wrapMode: Text.WordWrap
                        width: parent.width
                        onLinkActivated: { console.log(link); if(link.indexOf("mailto") > -1) Qt.openUrlExternally(link); else appLauncher.openURLDefault(link)}
                    }
                    Button {
                        id: aboutAppPrivPolicyButton
                        text: qsTr("Privacy Policy")
                        checkable: true
                        //width: parent.width
                    }
                    ListItemText {
                        text: Const.APP_PRIVACY_MESSAGE
                        role: "SubTitle"
                        visible: aboutAppPrivPolicyButton.checked
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: qsTr("Project Team")
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    Label {
                        text: "Yogeshwar P (twitter: @yogeshwarp)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Author")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Saurav Srivastava (twitter: @gx_saurav)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("UI Feedback & Refinement / Artwork<br>")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: qsTr("Translators")
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    Label {
                        text: "Yeatse (twitter: @yeatse)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Simplified Chinese")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Wei-Lin Chen (twitter: @garykb8)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Traditional Chinese")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Thorsten Taube"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("German Language")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Аспарух Калянджиев (twitter: @acnapyx)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Bulgarian Language")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Ville Makkonen"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Finnish Language")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Jerko Čilaš (twitter: @zvjer)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Croatian Language")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Paweł Gawlik (twitter: @pagaw102)"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Polish Language")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                    Label {
                        text: "Alessandro Pra'"
                        wrapMode: Text.Wrap
                        width: parent.width
                    }
                    ListItemText {
                        text: qsTr("Italian Language")
                        role: "SubTitle"
                        wrapMode: Text.Wrap
                        elide: Text.ElideNone
                        width: parent.width
                    }
                }
            }
        }
    }

    tools: ToolBarLayout {
        ToolIcon {
            //enabled: !pageStack.busy
            iconId: "toolbar-back"
            onClicked: pageStack.depth == 1 ? Qt.quit() : pageStack.pop()
        }
    }
}
