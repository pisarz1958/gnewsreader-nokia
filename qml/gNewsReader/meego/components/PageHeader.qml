/****************************************************************************
* This file is part of the Harmattan API Showcase Application.
*
* Copyright © 2012 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
*
* This software, including documentation, is protected by copyright
* controlled by Nokia Corporation.  All rights reserved.  You are
* entitled to use this file in accordance with the Harmattan API
* Showcase Application Agreement.
*
****************************************************************************/

import QtQuick 1.1
import com.nokia.meego 1.0

/*!
 * @brief Display the Header in the main page
 */
Image {
    id: headerholder

    property string text : ""

    height: parent.width < parent.height ? meegoStyle.graphicSizeLarge//uiConstants.HeaderDefaultHeightPortrait
                                         : meegoStyle.graphicSizeMedium//uiConstants.HeaderDefaultHeightLandscape
    width: parent.width
    source: "../../pics/meegotouch-feeds-header.png"//"image://theme/meegotouch-view-header-fixed" + (theme.inverted ? "-inverted" : "")
    x: 0; y: 0; z: 0
    visible: true//(rootWindow.pageStack.depth == 1) ? true : false

    //! Header container
    Item {
        anchors {
            fill: parent
            topMargin: parent.width < parent.height
                           ? meegoStyle.paddingMedium//uiConstants.HeaderDefaultTopSpacingPortrait
                           : meegoStyle.paddingMedium//uiConstants.HeaderDefaultTopSpacingLandscape
            bottomMargin: parent.width < parent.height
                              ? meegoStyle.paddingMedium//uiConstants.HeaderDefaultBottomSpacingPortrait
                              : meegoStyle.paddingMedium//uiConstants.HeaderDefaultBottomSpacingLandscape
            rightMargin: meegoStyle.paddingMedium//uiConstants.DefaultMargin
            leftMargin: meegoStyle.paddingMedium//uiConstants.DefaultMargin
        }

        //! Page header text
        Label {
            id: header
            anchors { verticalCenter: parent.verticalCenter; left: parent.left }
            font.family: meegoStyle.fontFamilyRegular//uiConstants.HeaderFont
            font.pixelSize: 28
            font.bold: true
            color: "white"
            text: headerholder.text
            elide: Text.ElideRight
        }
    }
}

//! End of file
