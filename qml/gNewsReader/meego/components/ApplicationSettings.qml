import QtQuick 1.1
import com.nokia.meego 1.0
import ".." 1.1

import "../../js/storage.js" as Storage
import "../../js/shareArticleService.js" as Script

Page {
    id: appSettingsPage
    property bool mainCompVisible : true
    property bool platformInverted : false

    anchors { fill: parent; topMargin: statusBar.height  + topMsgText.height; bottomMargin: toolBar.height }
    onStatusChanged: if(status == PageStatus.Active) { topMsgText.text=qsTr("Settings") }

    Loader {
        id: appSettingsLoader
        sourceComponent: appSettingsComponent//undefined
        anchors { fill: parent; margins: 0 }
        focus: true
    }

    Component {
        id:appSettingsComponent
        Flickable {
            width: parent.width
            height: parent.height
            contentWidth: parent.width
            contentHeight: mainColumnSettings.height
            boundsBehavior : Flickable.StopAtBounds
            Item {
                id: appSettingsItem
                width: parent.width
                Column {
                    id: mainColumnSettings
                    width: parent.width
                    ListItem {
                        ListItemText {text: qsTr("About gNewsReader"); anchors.centerIn: parent}
                        onClicked: { pageStack.push(Qt.resolvedUrl("AboutApplicationPage.qml")) }
                        width: parent.width
                    }
                    MenuItem {
                        text: qsTr("Use Light Theme")
                        Switch {
                            id: lightThemeSwitch
                            checked: window.useLightTheme
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('useLightTheme', checked)
                                window.useLightTheme = checked
                            }
                        }
                        width: parent.width
                        onClicked: lightThemeSwitch.checked = !lightThemeSwitch.checked//lightThemeSwitch.clicked()
                    }
                    MenuItem {
                        text: qsTr("Unread Filter Global")
                        Switch {
                            id: unreadFilterGolbalSwitch
                            checked: window.globalUnreadFilter
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('globalUnreadFilter', checked)
                                window.globalUnreadFilter = checked
                            }
                        }
                        width: parent.width
                        onClicked: unreadFilterGolbalSwitch.checked = !unreadFilterGolbalSwitch.checked//unreadFilterGolbalSwitch.clicked()
                    }
                    MenuItem {
                        text: qsTr("Auto Image Resize")
                        Switch {
                            id: autoResizeImageSwitch
                            checked: window.autoResizeImg
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('autoResizeImg', checked)
                                window.autoResizeImg = checked
                            }
                        }
                        width: parent.width
                        onClicked: autoResizeImageSwitch.checked = !autoResizeImageSwitch.checked//.clicked()
                    }
                    MenuItem {
                        text: qsTr("Full HTML Content")
                        Switch {
                            id: showFullHtmlSwitch
                            checked: window.showFullHtml
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('showFullHtml', checked)
                                window.showFullHtml = checked
                            }
                        }
                        width: parent.width
                        onClicked: showFullHtmlSwitch.checked = !showFullHtmlSwitch.checked
                    }
                    MenuItem {
                        text: qsTr("Use Bigger Fonts")
                        Switch {
                            id: useBiggerFontsSwitch
                            checked: window.useBiggerFonts
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('useBiggerFonts', checked)
                                window.useBiggerFonts = checked
                            }
                        }
                        width: parent.width
                        onClicked: useBiggerFontsSwitch.checked = !useBiggerFontsSwitch.checked
                    }
                    MenuItem {
                        text: qsTr("Auto Load Images")
                        Switch {
                            id: autoLoadImagesSwitch
                            checked: window.autoLoadImages
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('autoLoadImages', checked)
                                window.autoLoadImages = checked
                            }
                        }
                        width: parent.width
                        onClicked: autoLoadImagesSwitch.checked = !autoLoadImagesSwitch.checked
                    }
                    MenuItem {
                        text: qsTr("Enable Swipe Gesture")
                        Switch {
                            id: swipeEnableSwitch
                            checked: window.swipeGestureEnabled
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                Storage.setSetting('swipeGestureEnabled', checked)
                                window.swipeGestureEnabled = checked
                            }
                        }
                        width: parent.width
                        onClicked: swipeEnableSwitch.checked = !swipeEnableSwitch.checked
                    }
                    MenuItem {
                        text: "English (Requires Restart)"
                        Switch {
                            id: forceEnglishEnableSwitch
                            checked: settings.getValue("main/language/forceenglish", false)
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                settings.setValue("main/language/forceenglish", checked)
                            }
                        }
                        width: parent.width
                        onClicked: forceEnglishEnableSwitch.checked = !forceEnglishEnableSwitch.checked
                    }
                    MenuItem {
                        text: qsTr("Clear Authorization Data")
                        enabled: false
                    }
                    MenuItem {
                        ButtonRow {
                            width: 0.9*parent.width
                            exclusive: false
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingMedium
                            ToolButton {
                                visible: Storage.getSetting("TWITTER_OAUTH_TOKEN") != null && Storage.getSetting("TWITTER_OAUTH_TOKEN") != ""
                                iconSource: "../../pics/twitter.svg"
                                onClicked: Script.cleanTwitterAuth()
                                platformStyle: ButtonStyle{ inverted: true }
                            }
                            ToolButton {
                                visible: Storage.getSetting("FACEBOOK_OAUTH_TOKEN") != null && Storage.getSetting("FACEBOOK_OAUTH_TOKEN") != ""
                                iconSource: "../../pics/facebook.svg"
                                onClicked: Script.cleanFacebookAuth()
                                platformStyle: ButtonStyle{ inverted: true }
                            }
                            ToolButton {
                                visible: Storage.getSetting("INSTAPAPER_OAUTH_TOKEN") != null && Storage.getSetting("INSTAPAPER_OAUTH_TOKEN") != ""
                                iconSource: "../../pics/instapaper.svg"
                                onClicked: Script.cleanInstapaperAuth()
                                platformStyle: ButtonStyle{ inverted: true }
                            }
                            ToolButton {
                                visible: Storage.getSetting("READLATER_USEARNAME_READ_IT_LATER") != null && Storage.getSetting("READLATER_USEARNAME_READ_IT_LATER") != ""
                                iconSource: "../../pics/read_it_later.svg"
                                onClicked: Script.cleanPocketAuth()
                                platformStyle: ButtonStyle{ inverted: true }
                            }

                        }
                    }
                    MenuItem {
                        text: "Dark Article Theme"
                        Switch {
                            id: useDarkArticleViewEnableSwitch
                            checked: settings.getValue("main/settings/darkarticleview", true)
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onCheckedChanged:  {
                                settings.setValue("main/settings/darkarticleview", checked)
                            }
                        }
                        width: parent.width
                        onClicked: useDarkArticleViewEnableSwitch.checked = !useDarkArticleViewEnableSwitch.checked
                    }
                    MenuItem {
                        text: qsTr("Export OPML file (Requires Google Login)")
                        width: parent.width
                        onClicked: appLauncher.openURLDefault("http://www.google.com/reader/subscriptions/export?hl=en")
                    }
                }
            }
        }
    }

    tools: ToolBarLayout {
        ToolIcon {
            //enabled: !pageStack.busy
            iconId: "toolbar-back"
            onClicked: pageStack.depth == 1 ? Qt.quit() : pageStack.pop()
        }
    }
}
