import QtQuick 1.1
import com.nokia.meego 1.0
import ".."

import "../../js/OAuthConstants.js" as Const
import "../../js/storage.js" as Storage
import "../../js/shareArticleService.js" as Script

CommonDialog {
    id: loginDialog
    property string loginDialogTitle: qsTr("Login to ") + (service == "READ_IT_LATER"? qsTr("Pocket") : (service == "TWITTER"? qsTr("Twitter") : qsTr("Instapaper")) )
    titleText: editTextMode ? qsTr("Share Article with URL") : loginDialogTitle
    //titleIcon: (!editTextMode ? "" : (service == "TWITTER"? "../../pics/twitter.svg" : "../../pics/facebook.svg") )

    property string service: ""
    property string loginId: ""
    property string pwd: ""
    property string shareurl: ""
    property string sharetitle: ""
    property bool saveLogin: false
    property bool editTextMode: (service == "TWITTER" && oauthToken != null && oauthToken != "") || (service == "FACEBOOK" && oauthToken != null)
    property string oauthToken : ""
    property string oauthTokenSecret : ""

    content: Column {
        id: readLaterDialogColumn
        width: parent.width - 2*meegoStyle.paddingMedium
        height: childrenRect.height + meegoStyle.paddingMedium
        spacing: meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        TextField {
            placeholderText: qsTr("Login Id")
            id: loginIdText
            text: loginId
            width: parent.width
            visible: !editTextMode && !privacyPolicyButton.checked
        }
        TextField {
            placeholderText: qsTr("Password")
            id: pwdText
            text: pwd
            width: parent.width
            echoMode: TextInput.Password
            visible: !editTextMode && !privacyPolicyButton.checked
        }
        CheckBox {
            id: saveLoginCheckbox
            platformStyle: CheckBoxStyle { inverted: true }
            text: qsTr("Save Login Information")
            checked: saveLogin
            visible: service == "READ_IT_LATER" && !editTextMode && !privacyPolicyButton.checked
        }
        Button {
            id: privacyPolicyButton
            text: qsTr("Privacy Policy")
            checkable: true
            platformStyle: ButtonStyle { inverted: true }
            //onClicked: Script.goToPrivacyPage()
            visible: !editTextMode
        }
        Flickable {
            id: privPolicyFlickable
            visible: privacyPolicyButton.checked
            //opacity: privacyPolicyButton.checked ? 1 : 0
            width: parent.width
            height: privacyPolicyButton.checked ? (window.inPortrait ? 300 : 180) : 0
            contentHeight: privacyPolicyLabel.height
            contentWidth: privacyPolicyLabel.width
            flickableDirection: Flickable.VerticalFlick
            clip: true
            Label {
                id: privacyPolicyLabel
                text: Const.APP_PRIVACY_MESSAGE
                width: readLaterDialogColumn.width - 2*meegoStyle.paddingMedium
                wrapMode: Text.Wrap
                platformStyle: LabelStyle { inverted: true }
            }
        }
        TextArea {
            id: sharePostText
            placeholderText: qsTr("Enter Text Here")
            text: loginDialog.sharetitle
            textFormat : TextEdit.PlainText
            width: parent.width
            height: (screen.currentOrientation == Screen.Portrait || screen.currentOrientation == Screen.PortraitInverted) ? 140 : 100
            property int textLimit: service == "TWITTER" ? 118 : 500
            visible: editTextMode
            onTextChanged: if(text.length > textLimit) { text = text.substring(0, textLimit); cursorPosition = textLimit }

            Label {
                anchors.fill: parent
                opacity: 0.4
                id: textCounter
                text: sharePostText.textLimit - sharePostText.text.length
                visible: editTextMode && service == "TWITTER"
                color: meegoStyle.colorNormalMid
                font.pixelSize: parent.height - 2*meegoStyle.paddingMedium
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
        Label {
            id: shareUrl
            text: loginDialog.shareurl
            wrapMode: Text.Wrap
            elide: Text.ElideNone
            visible: editTextMode
            width: parent.width
            color: meegoStyle.colorNormalLink
        }
    }

    buttons: Row  {
        spacing: meegoStyle.paddingMedium
        id: rltbRow
        //exclusive: false
        width: parent.width - 2*meegoStyle.paddingMedium
        height: readLaterDialogSendButton.height + meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        Button {
            width: (rltbRow.width - 2*meegoStyle.paddingMedium)*0.15
            visible: loginDialog.editTextMode && !privacyPolicyButton.checked
            platformStyle: ButtonStyle{ inverted: true }
            iconSource: "../../pics/tb_delete.svg"
            onClicked: { if(loginDialog.service == "TWITTER") { Script.cleanTwitterAuth() } else Script.cleanFacebookAuth(); loginDialog.close() }
        }
        Button {
            width: (rltbRow.width - 2*meegoStyle.paddingMedium)*(editTextMode ? 0.45 : 0.5)
            id: readLaterDialogSendButton
            visible: !privacyPolicyButton.checked

            text: editTextMode ? qsTr("Send") : qsTr("Login")
            platformStyle: ButtonStyle{ inverted: true }
            onClicked: {
                loginDialog.close()
                if(loginDialog.service == "TWITTER") {
                    if(loginDialog.editTextMode) Script.tweetArticle(sharePostText.text + " " + loginDialog.shareurl, loginDialog.oauthToken, loginDialog.oauthTokenSecret)
                    else Script.makeTwitterAuthCall(loginIdText.text, pwdText.text, loginDialog.sharetitle, loginDialog.shareurl)
                } else if(loginDialog.service == "FACEBOOK") {
                    Script.facebookStatusUpdate(sharePostText.text, loginDialog.shareurl, loginDialog.oauthToken )
                } else if(loginDialog.service == "INSTAPAPER") {
                    Script.makeInstapaperAuthCall(loginIdText.text, pwdText.text, loginDialog.sharetitle, loginDialog.shareurl)
                } else {
                    if(loginIdText.text != undefined && loginIdText.text != "") {
                        feedItemPage.callSentToReadLater(service, loginIdText.text, pwdText.text, loginDialog.sharetitle, loginDialog.shareurl)
                        if(saveLoginCheckbox.checked) feedItemPage.saveAuthData(service, loginIdText.text, pwdText.text)
                    }
                }
            }
        }
        Button {
            width: (rltbRow.width - 2*meegoStyle.paddingMedium)*(editTextMode ? 0.4 : 0.5)
            visible: !privacyPolicyButton.checked
            text: qsTr("Cancel")
            platformStyle: ButtonStyle{ inverted: true }
            onClicked: loginDialog.close()
        }
    }
    //onClickedOutside: loginDialog.close()
}

