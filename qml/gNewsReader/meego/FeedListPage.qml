/*
    Copyright 2011 - Yogeshwar Padhyegurjar

    This file is part of gNewsReader.

    gNewsReader is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gNewsReader is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with gNewsReader. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 1.1
import com.nokia.meego 1.0

import "../js/OAuthConstants.js" as Const

Page {
    id: feedListPage
    property ListModel model
    property string continueId: ""
    property string feedUrl: ""
    property string feedExclude: ""
    property string feedTitle: ""
    property string sinceTime: ""
    property int listViewIndex:0

    property bool filterActive: feedListPage.feedExclude == "user/-/state/com.google/read"
    property bool mainCompVisible : (feedListPage.model != null && feedListPage.model.count > 0)
    property bool multiSelectMode : false

    signal itemClicked( string itemIndex )
    signal takeToTop()
    signal takeFocus()
    signal loadMoreItems(string feedUrl, string feedExclude, string continueId)

    onTakeToTop: if(feedListLoader.item != null) feedListLoader.item.takeToTop()
    onTakeFocus: if(feedListLoader.item != null) feedListLoader.item.takeFocus()
    onStatusChanged: {
        if(status == PageStatus.Activating) {if (feedListLoader.sourceComponent == undefined) feedListLoader.sourceComponent = feedListComponent}
        if(status == PageStatus.Active) topMsgText.text = (filterActive? "!" : "")+feedListPage.feedTitle
        if(status == PageStatus.Inactive) feedListLoader.sourceComponent = undefined
    }

    Loader {
        id: feedListLoader
        sourceComponent: undefined
        anchors { fill: parent; margins: 0 }
        focus: true
    }

    Component {
        id:feedListComponent
        Item {
            signal takeToTop()
            signal takeFocus()
            onTakeFocus: feedListView.forceActiveFocus()
            onTakeToTop: feedListView.positionViewAtIndex(0, ListView.Beginning)
            focus: true

            Keys.onPressed: {
                if (event.key == Qt.Key_Backspace) {
                    feedListPageBackButton.clicked()
                }
            }

            ListView {
                id: feedListView
                focus: true
                delegate: feedDelegate
                anchors { fill: parent; margins: 0 }
                model: feedListPage.model
                highlightFollowsCurrentItem: true
                header: feedListHeader
                footer: feedListFooter
                clip: true

                Component {
                    id: feedListHeader
                    PullToActivate {
                        myView: feedListView
                        onRefresh: feedListPageTButtonRefresh.clicked();
                        platformInverted: window.useLightTheme
                    }
                }

                Component {
                    id: feedListFooter
                    PullToActivate {
                        myView: feedListView
                        isHeader : false
                        visible: continueId != "ALLLOADED"
                        platformInverted: window.useLightTheme
                        onRefresh: if(visible) feedListPage.loadMoreItems(feedListPage.feedUrl, feedListPage.feedExclude, feedListPage.continueId)
                    }
                }
            }

//            ScrollBar {
//                id: vertical
//                flickableItem: feedListView
//                orientation: Qt.Vertical
//                anchors { right: feedListView.right; top: feedListView.top }
//            }
            ScrollDecorator {
                id: scrolldecorator
                flickableItem: feedListView
            }
            Component.onCompleted: { feedListView.forceActiveFocus(); if(feedListPage.listViewIndex != 0) { feedListView.positionViewAtIndex(feedListPage.listViewIndex, ListView.Center); feedListView.currentIndex = feedListPage.listViewIndex; feedListPage.listViewIndex = 0 } }
        }
    }

    Component {
        id: feedDelegate

        ListItem {
            id: newsfeedItem
            width: newsfeedItem.ListView.width//feedListView.width
            height: feedItemColumn.columnHeight + 2*meegoStyle.paddingMedium
            //platformInverted: window.useLightTheme

//            BorderImage {
//                asynchronous: true
//                visible: !readstatus && newsfeedItem.mode == "normal"
//                source: window.useLightTheme?"../pics/list_default_feed_inverse.svg":"../pics/list_default_feed.svg"
//                //border { left: 10; top: 0; right: 0; bottom: 0 }
//                smooth: false
//                anchors.fill: parent
//            }

            Rectangle {
                color: "#1080DD"
                visible: !readstatus && newsfeedItem.mode == "normal"
                anchors.left: parent.left
                anchors.top: parent.top
                height: parent.height
                width: meegoStyle.paddingSmall
            }

            Column {
                id: feedItemColumn
                property int columnHeight: height + anchors.topMargin
                                             + anchors.bottomMargin
                spacing: meegoStyle.paddingMedium
                anchors {
                    top: newsfeedItem.top
                    left: newsfeedItem.left
                    margins: meegoStyle.paddingMedium
                }
                width: newsfeedItem.ListView.width - 2*meegoStyle.paddingMedium

               ListItemText {
                    id: newsFeedTitle
                    mode: newsfeedItem.mode
                    text: /*"        "+*/title
                    width: newsfeedItem.width - 2*meegoStyle.paddingMedium//parent.width
                    maximumLineCount: 3
                    font.pixelSize: window.fontSizeLarge
                    wrapMode: Text.Wrap
                    font.family: readstatus ? meegoStyle.fontFamilyLight : meegoStyle.fontFamilyRegular
                }

                Row {
                    width: parent.width //newsfeedItem.width - 2*meegoStyle.paddingMedium
                    ListItemText {
                        id: newsFeedSrc
                        mode: newsfeedItem.mode
                        role: "SubTitle"
                        text: source
                        width: parent.width - itemStatusBar.width
                        font.pixelSize: window.fontSizeMedium
                        font.family: meegoStyle.fontFamilyLight
                    }

                    Row {
                        id: itemStatusBar
                        anchors.bottom: newsFeedSrc.bottom
                        ListItemText {
                            id: newsFeedTime
                            mode: newsfeedItem.mode
                            role: "SubTitle"
                            text: Const.humaneDate(feedTime*1)
                            font.pixelSize: window.fontSizeMedium
                            font.family: meegoStyle.fontFamilyLight
                        }
                        Image {
                            asynchronous: true
                            source: window.useLightTheme?"../pics/tb_favourite_inverse.svg":"../pics/tb_favourite.svg"
                            visible: starred
                            sourceSize.height: meegoStyle.graphicSizeTiny
                            sourceSize.width: meegoStyle.graphicSizeTiny
                        }
//                        Image {
//                            asynchronous: true
//                            source: window.useLightTheme?"../pics/tb_share_inverse.svg":"../pics/tb_share.svg"
//                            visible: shared
//                            sourceSize.height: meegoStyle.graphicSizeTiny
//                            sourceSize.width: meegoStyle.graphicSizeTiny
//                        }
                    }
                }
            }

            onClicked: feedListPage.itemClicked(index)
            onPressAndHold: {
                var optionMenu = Qt.createComponent("components/FeedListItemOptions.qml").createObject(feedListPage)
                optionMenu.feedIndex = index; optionMenu.feedId = feedId; optionMenu.feedUrl = feedListPage.feedUrl; optionMenu.starred = starred; optionMenu.readstatus = readstatus; optionMenu.keptUnread = keptUnread; optionMenu.categories = categories
                optionMenu.title = title; optionMenu.articleUrl = articleUrl
                optionMenu.open()
            }
        }
    }
}
