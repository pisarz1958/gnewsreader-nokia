/*
    Copyright 2011 - Yogeshwar Padhyegurjar

    This file is part of gNewsReader.

    gNewsReader is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gNewsReader is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with gNewsReader. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1

Page {
    id: subscrListPage
    property ListModel model
    property bool mainCompVisible : (subListModel != null && subListModel.count > 0)
    property int feedSectionToFocus: 0

    signal folderClicked(int index, string folderId, bool expanded)
    signal itemClicked( string itemId, string feedTitle )
    signal longPressMenu (string inId, string inTitle, int index, bool isHeader, string cat, string allcategories)
    signal itemOptions( string itemId, string listIndex, string menuId, string feedTitle )
    signal showSubDialog(string feed, string title)
    signal searchForFeed(string query)

    signal takeToTop()
    signal takeFocus()
    onTakeToTop: if(subCompLoader.item != null) subCompLoader.item.takeToTop()
    onTakeFocus: if(subCompLoader.item != null) subCompLoader.item.takeFocus()
    onStatusChanged: {
        if(status == PageStatus.Activating) {if (subCompLoader.sourceComponent == undefined) {subCompLoader.sourceComponent = subListComponent;} }
        if(status == PageStatus.Active) { topMsgText.text=qsTr("Subscriptions"); subCompLoader.item.takeFocus() }
        if(status == PageStatus.Inactive) subCompLoader.sourceComponent = undefined
    }
    onShowSubDialog: { var addNewFeedComp = Qt.createComponent("components/AddNewFeedDialog.qml"); addNewFeedComp.createObject(subscrListPage).open()}//subFeedDialogComp.createObject(subscrListPage).open()

    onLongPressMenu: {
        //var optionMenu = itemOptionMenuComp.createObject(subscrListPage)
        var optionMenu = Qt.createComponent("components/SubItemOptionsMenu.qml").createObject(subscrListPage)
        optionMenu.itemIndex = index; optionMenu.feedId = inId; optionMenu.feedTitle = inTitle; optionMenu.isSection = isHeader; optionMenu.category = cat; optionMenu.allcats = allcategories
        optionMenu.open()
    }

    Loader {
        id: subCompLoader
        sourceComponent: undefined//subListComponent
        anchors { fill: parent; margins: 0 }
        focus: true
    }

    Component {
        id:subListComponent
        Item {
            id: sublistItem
            focus: true
            signal takeToTop()
            signal takeFocus()
            onTakeToTop: subListView.positionViewAtIndex(0, ListView.Beginning)
            onTakeFocus: subListView.forceActiveFocus()

            ListView {
                id: subListView
                focus: true
                delegate: listDelegate
                width: parent.width
                clip: true
                anchors { fill: parent; margins: 0 }
                model: subListModel//subListModelFilter
                visible: subscrListPage.mainCompVisible
                highlightFollowsCurrentItem: true

                header: subListHeader

                Component {
                    id: subListHeader
                    PullToActivate {
                        myView: subListView
                        platformInverted: window.useLightTheme
                        onRefresh: subListPageTButtonRefresh.clicked()
                    }
                }
            }
            ScrollDecorator {
                id: subscrolldecorator
                flickableItem: subListView
            }
//            ScrollBar {
//                id: vertical
//                flickableItem: subListView
//                orientation: Qt.Vertical
//                anchors { right: subListView.right; top: subListView.top }
//            }
            Component.onCompleted: { subListView.forceActiveFocus(); if(feedSectionToFocus != 0) { subListView.positionViewAtIndex((feedSectionToFocus*subListModel.count)/100,ListView.Center); feedSectionToFocus = 0 } }
        } 
    }

    Component {
        id: listDelegate
        ListItem {
            id: sublistItem
            //visible: count > 0
            width: sublistItem.ListView.width//subListView.width
            height: subItemTitle.itemTextHeight + 2*meegoStyle.paddingLarge
            //platformInverted: window.useLightTheme

            BorderImage {
                asynchronous: true
                visible: !sub && sublistItem.mode == "normal"
                source: window.useLightTheme?"../pics/list_default_inverse.svg":"../pics/list_default.svg"
//                border { left: 0; top: 0; right: 0; bottom: 0 }
                smooth: false
                anchors.fill: parent
            }

            Row {
                id: subRow
                width: sublistItem.ListView.width - 2*meegoStyle.paddingMedium//parent.width
                anchors.fill: sublistItem.paddingItem
                spacing: meegoStyle.paddingMedium

                Image {
                    id: itemImage
                    asynchronous: true
                    visible: sub
                    source: window.useLightTheme?"../pics/tb_feed_inverse.svg":"../pics/tb_feed.svg"
                    sourceSize.height: meegoStyle.graphicSizeSmall
                    sourceSize.width: meegoStyle.graphicSizeSmall
                    anchors.verticalCenter: subItemTitle.verticalCenter
                }

                Image {
                    id: tagImage
                    asynchronous: true
                    visible: cat === "tag"
                    property bool isExpanded: true
                    source: window.useLightTheme?"../pics/tb_tag_inverse.svg":"../pics/tb_tag.svg"
                    sourceSize.height: meegoStyle.graphicSizeSmall
                    sourceSize.width: meegoStyle.graphicSizeSmall
                    anchors.verticalCenter: subItemTitle.verticalCenter
                }

                Image {
                    id: folderImage
                    asynchronous: true
                    visible: cat === "folder"
                    property bool isExpanded: true
                    source: window.useLightTheme?"../pics/tb_folder_inverse.svg":"../pics/tb_folder.svg"
                    sourceSize.height: meegoStyle.graphicSizeSmall
                    sourceSize.width: meegoStyle.graphicSizeSmall
                    anchors.verticalCenter: subItemTitle.verticalCenter
                    MouseArea {
                        enabled: cat == "folder"
                        anchors.fill: parent
                        onClicked: { subscrListPage.folderClicked(index, itemId, folderImage.isExpanded); folderImage.isExpanded = !folderImage.isExpanded; }
                    }
                }

                ListItemText {
                    id: subItemTitle

                    property int itemTextHeight: height + anchors.topMargin
                                                 + anchors.bottomMargin
                    mode: sublistItem.mode
                    font.pixelSize: sub ? window.fontSizeMedium : window.fontSizeLarge
                    font.family: sub ? meegoStyle.fontFamilyRegular : meegoStyle.fontFamilyLight
                    text: title
                    width: (parent.width - itemImage.width - subItemCount.width - 2*meegoStyle.paddingMedium)
                    verticalAlignment: Text.AlignVCenter
                }

                CountBubble  {
                    id: subItemCount
                    value: count
                    largeSized: count > 0
//                    text: count
//                    mode: sublistItem.mode
//                    role: "Heading"
//                    horizontalAlignment: Text.AlignRight
//                    verticalAlignment: Text.AlignVCenter
//                    font.family: meegoStyle.fontFamilyLight
                }
            }
            onClicked: { subscrListPage.feedSectionToFocus = (index/subListModel.count)*100; if(itemId != "zzunknown") subscrListPage.itemClicked(itemId, title)}
            onPressAndHold: subscrListPage.longPressMenu(itemId, title, index, !sub, cat, allcategories)
        }
    }
}
