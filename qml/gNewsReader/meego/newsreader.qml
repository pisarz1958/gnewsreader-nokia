/*
    Copyright 2011 - Yogeshwar Padhyegurjar

    This file is part of gNewsReader.

    gNewsReader is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gNewsReader is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with gNewsReader. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1
import "./components" 1.1

import "../js/OAuthConstants.js" as Const
import "../js/storage.js" as Storage
import "../js/main.js" as Script

Window {
    id: window
    property bool busyInd: false
    property bool autoResizeImg: true
    property bool showFullHtml: false
    property bool showToolBar: true
    property bool globalUnreadFilter: false
    property bool useLightTheme: false
    property bool useBiggerFonts: false
    property bool autoLoadImages: true
    property bool swipeGestureEnabled: true
    property bool isOnline: false

    property int fontSizeSmall: useBiggerFonts ? 1.1*meegoStyle.fontSizeSmall : meegoStyle.fontSizeSmall
    property int fontSizeMedium: useBiggerFonts ? 1.1*meegoStyle.fontSizeMedium : meegoStyle.fontSizeMedium
    property int fontSizeLarge: useBiggerFonts ? 1.1*meegoStyle.fontSizeLarge : meegoStyle.fontSizeLarge

    onUseLightThemeChanged: theme.inverted = !window.useLightTheme
    Component.onDestruction: Script.saveAllSubscriptions()

//    Connections {
//        target: appLauncher
//        onNwStateChanged: window.isOnline = isOnline;//console.log("Online status changed to:"+ isOnline)
//    }

    property Style platformStyle: PageStackWindowStyle{}
    property alias style: window.platformStyle

    Rectangle {
        id: background
        visible: platformStyle.background == ""
        color: platformStyle.backgroundColor
        anchors { top: topMsgText.bottom; left: parent.left; bottom: parent.bottom; right: parent.right; }
    }

    Image {
        id: backgroundImage
        visible: platformStyle.background != ""
        source: window.inPortrait ? style.portraitBackground : style.landscapeBackground
        fillMode: style.backgroundFillMode
        anchors { top: statusBar.bottom; left: parent.left; bottom: parent.bottom; right: parent.right; }
    }

    PageStack {
        id: pageStack

        anchors.fill: parent
        toolBar: toolBar

        signal busyStart()
        signal busyStop()
        onBusyStart: Script.busyIndicatorStart()
        onBusyStop: Script.busyIndicatorStop()
    }

    PageHeader {
        id: topMsgText
        text: "gNewsReader"
        visible: true//(pageStack.busy && theme.inverted) ? false : true
        anchors.top: statusBar.bottom

        MouseArea {
            anchors.fill: parent
            onClicked: if(pageStack.currentPage.takeToTop != null) pageStack.currentPage.takeToTop()
            //enabled: pageStack.busy
        }

        BusyIndicator {
            id: globalBusy
            running: true
            visible: (window.busyInd && !centralMessageArea.visible && !aboutAppPrivPolicyButton.visible)
            platformStyle: BusyIndicatorStyle { size: "small"; inverted: false }
            anchors.right: parent.right
            anchors.rightMargin: meegoStyle.paddingMedium
            anchors.verticalCenter: parent.verticalCenter
        }

        Button {
            id: aboutAppPrivPolicyButton
            text: qsTr("Privacy Policy")
            visible: googleAuthPage.showPrivacyNotice
            onClicked: gnrdialog.createQueryDialog("", Const.APP_PRIVACY_MESSAGE, function(){})
            anchors.right: parent.right
            anchors.rightMargin: meegoStyle.paddingMedium
            width: 180
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    //Event preventer when page transition is active
    MouseArea {
        anchors.fill: parent
        enabled: pageStack.busy
    }

    StatusBar {
        id: statusBar
        anchors.top: parent.top
        width: parent.width
    }

    onOrientationChangeStarted: {
        statusBar.orientation = screen.currentOrientation
    }

    InfoBanner {
        id: pageInfoBanner
        anchors.top: statusBar.bottom
        width: screen.displayWidth
    }

    Item {
        id: centralMessageArea
        anchors { fill: parent; topMargin: statusBar.height; bottomMargin: toolBar.height }
        visible: !(pageStack.currentPage != null && pageStack.currentPage.mainCompVisible)

        BorderImage {
            source: window.useLightTheme? "../pics/list_default_inverse.svg" : "../pics/msg_background.svg"
            border { left: 15; top: 15; right: 15; bottom: 15 }
            smooth: true
            asynchronous: true
            anchors.centerIn: msgAreaColumn
            width: msgAreaColumn.width + 30
            height: msgAreaColumn.height + 30
        }
        Column {
            id: msgAreaColumn
            anchors.centerIn: parent
            spacing: meegoStyle.paddingMedium

            BusyIndicator {
                id: centralBusy
                visible: window.busyInd && centralMessageArea.visible
                running: window.busyInd
                anchors.horizontalCenter: parent.horizontalCenter
                height: meegoStyle.graphicSizeLarge
                width: meegoStyle.graphicSizeLarge
            }
            Label {
                id: infoMessage
                text: qsTr("gNewsReader")
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    Item {
        id: roundedCorners
        visible: true//platformStyle.cornersVisible
        anchors.fill: parent
        z: 10001

        Image {
            anchors.top : parent.top
            anchors.left: parent.left
            source: "image://theme/meegotouch-applicationwindow-corner-top-left"
        }
        Image {
            anchors.top: parent.top
            anchors.right: parent.right
            source: "image://theme/meegotouch-applicationwindow-corner-top-right"
        }
        Image {
            anchors.bottom : parent.bottom
            anchors.left: parent.left
            source: "image://theme/meegotouch-applicationwindow-corner-bottom-left"
        }
        Image {
            anchors.bottom : parent.bottom
            anchors.right: parent.right
            source: "image://theme/meegotouch-applicationwindow-corner-bottom-right"
        }
    }

    ToolBar {
        id: toolBar
        anchors.bottom: parent.bottom
        //platformStyle: ToolBarStyle { inverted: true }
        privateVisibility: (inputContext.softwareInputPanelVisible==true || inputContext.customSoftwareInputPanelVisible == true)
                    ? ToolBarVisibility.HiddenImmediately : (window.showToolBar ? ToolBarVisibility.Visible : ToolBarVisibility.Hidden)
        tools: ToolBarLayout {
            id: toolBarLayout
            ToolIcon {
                //flat: true
                iconId: "toolbar-back"
                onClicked: Qt.quit()
            }
        }
    }

    SubscriptionsPage {
        id: subscrListPage
        property string filtermode: Storage.getSettingVal("filtermode", "all")
        property bool isCountDirty: false

        model: ListModel {
            id: subListModel
        }

        onFolderClicked: Script.folderClicked(index, folderId, expanded)
        onItemClicked: Script.loadById(itemId, feedTitle)
        onItemOptions: Script.editSub(listIndex, itemId, menuId, feedTitle)
        onFiltermodeChanged: { if(filtermode!=Storage.getSettingVal("filtermode")) /*subListModelFilter.setMinimumCount(filtermode == "all" ? 0 : 1)*/Script.filterSubList(filtermode); Storage.setSetting("filtermode",filtermode) }
        onSearchForFeed: Script.searchForFeed(query)
        onStatusChanged: {
            if(status == PageStatus.Activating) {
                if(subscrListPage.isCountDirty) {
                    subscrListPage.isCountDirty = false; Script.updateSubscriptions()
                }
            }
        }

        anchors { fill: parent; topMargin: statusBar.height + topMsgText.height; bottomMargin: toolBar.height }

        tools: ToolBarLayout {
            ToolIcon {
                enabled: !pageStack.busy
                iconId: "toolbar-close"
                onClicked: Qt.quit()
            }
            ToolIcon {
                id: subListPageTButtonRefresh
                iconId: "toolbar-refresh"
                onClicked: Script.refreshSubList("count")
            }
            ToolIcon {
                iconId: "toolbar-settings"
                onClicked: pageStack.push(Qt.resolvedUrl("components/ApplicationSettings.qml"))
            }
            ToolIcon {
                iconId: "toolbar-view-menu"
                onClicked: (subListMenu.status == DialogStatus.Closed) ? subListMenu.open() : subListMenu.close()
            }
        }

//        Component {
//            id: subListMenuComp
            Menu {
                id: subListMenu
                onStatusChanged: if(status == DialogStatus.Closed) subscrListPage.takeFocus()
                visualParent: subscrListPage
                //platformStyle: MenuStyle {leftMargin:0; rightMargin:0; topMargin:0; bottomMargin:0 }
                MenuLayout {

//                    MenuItem {
//                        text: qsTr("Go Online")
//                        visible: !window.isOnline
//                        onClicked: appLauncher.openNetConnection()
//                    }
//                    MenuItem {
//                        text: qsTr("Go Offline")
//                        visible: window.isOnline
//                        onClicked: appLauncher.closeNetConnection()
//                    }
                    MenuItem {
                        text: qsTr("Subscribe to New Feed")
                        onClicked: subscrListPage.showSubDialog("","")
                    }
                    MenuItem {
                        text: subscrListPage.filtermode == "unread"? qsTr("Show All") : qsTr("Show Unread")
                        onClicked: subscrListPage.filtermode == "all" ? subscrListPage.filtermode = "unread": subscrListPage.filtermode = "all"
                    }
                    MenuItem {
                        text: qsTr("Load New Feeds")
                        onClicked: Script.loadUnreadNews()
                    }
                    MenuItem {
                        text: qsTr("Load All Feeds")
                        onClicked: Script.loadAllNews()
                    }
                    MenuItem {
                        text: qsTr("Load Starred Feeds")
                        onClicked: Script.loadStarred()
                    }
                    MenuItem {
                        text: qsTr("Clear Authorization Data")
                        onClicked: gnrdialog.createQueryDialog(qsTr("Confirm Action"), qsTr("Clear Authorization Data")  + "?", function() {Script.clearAuthData()})
                    }
                }
            }
        //}
    }

    FeedListPage {
        id: feedListPage
        model: ListModel {
            id: feedListModel
        }

        signal toggleTagStatus(int index, string feedId, string feedUrl, string tagName, bool currTagVal, string tagAct)
        onToggleTagStatus: Script.toggleTagStatus(tagAct, currTagVal, tagName, feedId, decodeURIComponent(feedUrl), index)
        signal updateFeedCount(string feedUrl, string categories, int count)
        onUpdateFeedCount: Script.updateCount(decodeURIComponent(feedUrl), categories, count)
        signal shareToReadLater(string serviceName, string feedTitle, string articleUrl)
        onShareToReadLater: Script.sendToReadLaterService(serviceName, feedTitle, articleUrl)
        onItemClicked: { Script.loadFeedDetails(itemIndex, true) }
	onLoadMoreItems: { Script.loadMore(feedUrl, feedExclude, continueId) }
        anchors { fill: parent; topMargin: statusBar.height + topMsgText.height; bottomMargin: toolBar.height }

        tools: ToolBarLayout {
            ToolIcon {
                id: feedListPageBackButton
                iconId: "toolbar-back"
                enabled: !pageStack.busy
                onClicked: {
                    pageStack.replace(subscrListPage)
                }
            }
            ToolIcon {
                id: feedListPageTButtonRefresh
                iconId: "toolbar-refresh"
                onClicked: Script.reloadNewsUrl(feedListPage.feedUrl, feedListPage.feedExclude, false, feedListPage.feedTitle)
            }
            ToolIcon {
                iconId: "toolbar-settings"
                onClicked: pageStack.push(Qt.resolvedUrl("components/ApplicationSettings.qml"))
            }
            ToolIcon {
                iconId: "toolbar-view-menu"
                onClicked: (feedListMenu.status == DialogStatus.Closed) ? feedListMenu.open() : feedListMenu.close()
                //onClicked: feedListMenuComp.createObject(pageStack).open()//feedListMenu.open();
            }
        }

//        Component {
//            id: feedListMenuComp
            Menu {
                id: feedListMenu
                onStatusChanged: if(status == DialogStatus.Closed) feedListPage.takeFocus()
                visualParent: feedListPage
                MenuLayout {
                    MenuItem {
                        visible: Script.getCount(decodeURIComponent(feedListPage.feedUrl)) > 0
                        text: qsTr("Mark All as Read")
                        onClicked: gnrdialog.createQueryDialog(qsTr("Confirm Action"), qsTr("Mark All as Read") + "?", function() {subscrListPage.itemOptions(decodeURIComponent(feedListPage.feedUrl), -1, "markAll", feedListPage.feedTitle)} )
                    }
                    MenuItem {
                        visible: feedListPage.continueId != "ALLLOADED"
                        text: qsTr("Load More Items..")
                        onClicked: Script.loadMore(feedListPage.feedUrl, feedListPage.feedExclude, feedListPage.continueId)
                    }
                    MenuItem {
                        text: feedListPage.filterActive? qsTr("Show All Items") : qsTr("Show Unread Items")
                        onClicked: {
                            if(feedListPage.feedExclude == "user/-/state/com.google/read") feedListPage.feedExclude = ""
                            else feedListPage.feedExclude = "user/-/state/com.google/read"
                            Script.reloadNewsUrl(feedListPage.feedUrl, feedListPage.feedExclude, false, feedListPage.feedTitle)
                        }
                    }
                }
            }
        //}
    }

    FeedItemPage {
        id: feedItemPage
        anchors { fill: parent; topMargin: statusBar.height  + topMsgText.height; bottomMargin: toolBar.height }

        onBackToList: { feedListPage.listViewIndex = feedItemPage.index; pageStack.replace(feedListPage) }
        onMarkFeedAsRead: Script.markAsRead(feedindex, replacePage)
        onSaveTags: Script.saveTags(tagsToSave, originalTags, feedItemPage.feedId, feedItemPage.feedUrl)
        onCallSentToReadLater : Script.invokeReadItLaterAddService(inSvc, inUser, inPwd, inTitle, inUrl)
        onSaveAuthData: Script.saveReadForLaterAuth(service, inUser, inPwd)

        signal shareArticle(string shareServiceUrl, string topStatusText, string shareTitle, string shareUrl)
        onShareArticle: {
            feedItemPage.startIndex = feedItemPage.index;
            pageStack.push(googleAuthPage); topMsgText.text = topStatusText
            googleAuthPage.urlString = shareServiceUrl
            googleAuthPage.shareTitle = shareTitle; googleAuthPage.shareUrl = shareUrl
        }

        tools: ToolBarLayout {
            ToolIcon {
                enabled: !pageStack.busy
                iconId: "toolbar-back"
                onClicked: feedItemPage.backToList()
            }
            ToolIcon {
                enabled: feedItemPage.index > 0
                iconId: "toolbar-previous"
                //flat: false
                onClicked: feedItemPage.loadPrev()
            }
            ToolIcon {
                enabled: feedItemPage.index < feedListModel.count - 1
                iconId: "toolbar-next"
                //flat:  false
                onClicked: feedItemPage.loadNext()
            }
//            ToolIcon {
//                iconId: "toolbar-settings"
//                onClicked: { feedItemPage.startIndex = feedItemPage.index; pageStack.push(Qt.resolvedUrl("components/ApplicationSettings.qml")) }
//            }
            ToolIcon {
                iconSource: feedItemPage.getCurrentFeed().keptUnread ? "../pics/tb_mark_read.svg" : "../pics/tb_mark_unread.svg"
                //onClicked: { feedItemPage.startIndex = feedItemPage.index; pageStack.push(Qt.resolvedUrl("../qml/components/ApplicationSettings.qml")) }
                onClicked: {
                    Script.toggleTagStatus(Const.KEEP_UNREAD_ACT, feedItemPage.getCurrentFeed().keptUnread, "keptUnread", feedItemPage.getCurrentFeed().feedId, feedItemPage.getCurrentFeed().feedUrl, feedItemPage.index)
                    Script.toggleTagStatus(Const.READ_ACT, feedItemPage.getCurrentFeed().readstatus, "readstatus", feedItemPage.getCurrentFeed().feedId, feedItemPage.getCurrentFeed().feedUrl, feedItemPage.index)
                    Script.updateCount(feedItemPage.getCurrentFeed().feedUrl, feedItemPage.getCurrentFeed().categories, feedItemPage.getCurrentFeed().keptUnread ? -1 : 1)
                }
            }
            ToolIcon {
                iconId: "toolbar-view-menu"
                onClicked: (feedItemMenu.status == DialogStatus.Closed) ? feedItemMenu.open() : feedItemMenu.close()
                //onClicked: feedItemMenuComp.createObject(pageStack).open()
            }
        }

//        Component {
//            id: feedItemMenuComp
            Menu {
                id: feedItemMenu
                onStatusChanged: if(status == DialogStatus.Closed) feedItemPage.takeFocus()
                visualParent: feedItemPage
                MenuLayout {
                    MenuItem {
                        //id: feedItemPageMenuOpenInBrowser
                        visible: feedItemPage.getCurrentFeed() != null && feedItemPage.getCurrentFeed().articleUrl != null && feedItemPage.getCurrentFeed().articleUrl != ""
                        text: qsTr("Open in Browser")
                        onClicked: appLauncher.openURLDefault(feedItemPage.getCurrentFeed().articleUrl)
                        ToolButton {
                            //iconSource: "../pics/tb_copy.svg"
                            text: qsTr("Copy URL")
                            width: 0.4*parent.width
                            flat: false
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            onClicked: {
                                var textForCopy = Qt.createQmlObject('import QtQuick 1.1; TextInput {text: ""; visible:false}', feedItemPage, "forCopy");
                                textForCopy.text = feedItemPage.getCurrentFeed().articleUrl; textForCopy.selectAll(); textForCopy.copy(); textForCopy.destroy();
                                feedItemMenu.close()
                            }
                        }
                    }
//                    MenuItem {
//                        text: (feedItemPage.getCurrentFeed() != null && feedItemPage.getCurrentFeed().keptUnread) ? qsTr("Undo Keep Unread"): qsTr("Keep Unread")

//                        onClicked: {
//                            Script.toggleTagStatus(Const.KEEP_UNREAD_ACT, feedItemPage.getCurrentFeed().keptUnread, "keptUnread", feedItemPage.getCurrentFeed().feedId, feedItemPage.getCurrentFeed().feedUrl, feedItemPage.index)
//                            Script.toggleTagStatus(Const.READ_ACT, feedItemPage.getCurrentFeed().readstatus, "readstatus", feedItemPage.getCurrentFeed().feedId, feedItemPage.getCurrentFeed().feedUrl, feedItemPage.index)
//                            Script.updateCount(feedItemPage.getCurrentFeed().feedUrl, feedItemPage.getCurrentFeed().categories, feedItemPage.getCurrentFeed().keptUnread ? -1 : 1)
//                        }
//                    }
                    MenuItem {
                        //text: qsTr("Share")
                        ButtonRow {
                            width: 0.9*parent.width
                            exclusive: false
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            ToolButton {
                                iconSource: "../pics/twitter.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: { /*feedItemPage.shareArticle(Const.getTwitterShareUrl(feedItemPage.getCurrentFeed().articleUrl, feedItemPage.getCurrentFeed().title), "Share (Twitter)"); feedItemMenu.close()*/
                                    Script.sendToReadLaterService("TWITTER", feedItemPage.getCurrentFeed().title, feedItemPage.getCurrentFeed().articleUrl)
                                    feedItemMenu.close()
                                }
                            }
                            ToolButton {
                                iconSource: "../pics/facebook.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: { /*feedItemPage.shareArticle(Const.getFacebookShareUrl(feedItemPage.getCurrentFeed().articleUrl, feedItemPage.getCurrentFeed().title), "Share (Facebook)"); feedItemMenu.close()*/
                                    Script.sendToReadLaterService("FACEBOOK", feedItemPage.getCurrentFeed().title, feedItemPage.getCurrentFeed().articleUrl)
                                    feedItemMenu.close()
                                }
                            }
                            ToolButton {
                                iconSource: "../pics/read_it_later.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: {
                                    Script.sendToReadLaterService(Const.SERVICE_POCKET, feedItemPage.getCurrentFeed().title, feedItemPage.getCurrentFeed().articleUrl)
                                    feedItemMenu.close()
                                }
                            }
                            ToolButton {
                                iconSource: "../pics/instapaper.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: {
                                    Script.sendToReadLaterService(Const.SERVICE_INSTAPAPER, feedItemPage.getCurrentFeed().title, feedItemPage.getCurrentFeed().articleUrl)
                                    feedItemMenu.close()
                                }
                            }
                            ToolButton {
                                iconSource: "../pics/tb_email.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: Qt.openUrlExternally("mailto:?subject="+encodeURIComponent(feedItemPage.getCurrentFeed().title)+"&body="+encodeURIComponent(feedItemPage.getCurrentFeed().title + " "+feedItemPage.getCurrentFeed().articleUrl))
                            }
//                            ToolIcon {
//                                iconSource: "../pics/tb_messaging.svg"
//				platformStyle: ButtonStyle{ inverted: true }
//                            }
                        }
                    }
                    MenuItem {
                        text: qsTr("Google")
                        ButtonRow {
                            width: 0.7*parent.width
                            exclusive: false
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: meegoStyle.paddingLarge
                            ToolButton {
                                iconSource: "../pics/tb_favourite.svg"
                                checkable: true
                                platformStyle: ButtonStyle{ inverted: true }
                                checked: feedItemPage.getCurrentFeed() != null && feedItemPage.getCurrentFeed().starred
                                onClicked: { Script.toggleTagStatus(Const.STAR_ACT, feedItemPage.getCurrentFeed().starred, "starred", feedItemPage.getCurrentFeed().feedId, feedItemPage.getCurrentFeed().feedUrl, feedItemPage.index); feedItemMenu.close() }
                            }
                            ToolButton {
                                iconSource: "../pics/tb_tag.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: { feedItemPage.showEditTagDialog(); feedItemMenu.close() }
                            }
                            ToolButton {
                                iconSource: "../pics/googleplus.png"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: { feedItemPage.shareArticle(Const.getGooglePlusShareUrl(feedItemPage.getCurrentFeed().articleUrl, feedItemPage.getCurrentFeed().title), "Share (Google+)"); feedItemMenu.close() }
                            }
                            ToolButton {
                                iconSource: "../pics/tb_like.svg"
                                platformStyle: ButtonStyle{ inverted: true }
                                onClicked: { feedItemPage.shareArticle(Const.getGooglePlusOneUrl(feedItemPage.getCurrentFeed().articleUrl), "+1 (Google+)"); feedItemMenu.close() }
                            }
                        }
                     }
                }
            }
        //}
    }

    GoogleOAuth2 {
        id: googleAuthPage
        anchors { fill: parent; topMargin: statusBar.height  + topMsgText.height; bottomMargin: toolBar.height }

        onAuthComplete: Script.oAuthComplete(token, refreshtoken)

        tools: ToolBarLayout {
            ToolIcon {
                visible: pageStack.depth > 1
                enabled: !pageStack.busy
                iconId: "toolbar-back"
                onClicked: pageStack.pop()
            }
            ToolIcon {
                visible: pageStack.depth == 1
                enabled: !pageStack.busy
                iconId: "toolbar-close"
                onClicked: Qt.quit()
            }
            ToolIcon {
                iconSource: "../pics/tb_zoom_out.svg"
                onClicked: googleAuthPage.zoomOut()
            }
            ToolIcon {
                iconSource: "../pics/tb_zoom_in.svg"
                onClicked: googleAuthPage.zoomIn()
            }
        }
    }

    Component.onCompleted: { Script.startApp(true); theme.inverted = !window.useLightTheme }

    QtObject{
        id: gnrdialog

        property Component queryDialog: null

        function createQueryDialog(titleText, message, acceptCallback){
            if(!queryDialog) queryDialog = Qt.createComponent("components/CommonQueryDialog.qml")
            var prop = { titleText: titleText, message: message }
            var dialog = queryDialog.createObject(pageStack.currentPage, prop)
            dialog.accepted.connect(acceptCallback)
        }
    }
}
